# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 11:19:37 2019

@author: KIIT
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
#importing data
dataset = pd.read_csv('acces.csv')
x = dataset.iloc[:, 1:6].values 
y = dataset.iloc[:, [6]].values

#For splitting data
"""from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test = train_test_split(x, y, test_size=0.2, random_state=0 )"""
#For feature scaling
"""
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
sc_y = StandardScaler()
x = sc_x.fit_transform(x)
y = sc_y.fit_transform(y)"""

#sklearn.ensemble is the library
#sklearn - to perform high mathematical calc.
#ensemble- for performing same alg. multiple time or constitutes of diff alg. to hide individual alg. drawbacks
#fitting dataset to ForestRegression
from sklearn.ensemble import RandomForestRegressor
regressor = RandomForestRegressor(n_estimators = 300, random_state = 0)
regressor.fit(x, y)

#Predicting a new result by FTR regression
y_pred = regressor.predict(np.array([1,1,0,1,1],ndmin=2))


