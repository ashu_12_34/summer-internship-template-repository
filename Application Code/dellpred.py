# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 22:58:49 2019

@author: -
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
#importing data
dataset = pd.read_csv('Recom_pred.csv')
x = dataset.iloc[:, 1:5].values 
y = dataset.iloc[:, [5]].values

#For splitting data
"""from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test = train_test_split(x, y, test_size=0.2, random_state=0 )"""
#For feature scaling
"""
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
sc_y = StandardScaler()
x = sc_x.fit_transform(x)
y = sc_y.fit_transform(y)"""
 
#fitting dataset to RandomForest
from sklearn.ensemble import RandomForestRegressor
regressor = RandomForestRegressor(n_estimators = 300, random_state = 0)
regressor.fit(x, y)

#Predicting a new result by FTR regression
y_pred = regressor.predict(np.array([2,2,2,4],ndmin=2))

